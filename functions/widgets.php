<?php

class FilmWidget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'Films Widget' );
	}

	function widget( $args, $instance ) {
		echo do_shortcode( '[get_films count=5]' ); 
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
	}

	function form( $instance ) {
		// Output admin widget options form
	}
}

function film_register_widgets() {
	register_widget( 'FilmWidget' );
}

add_action( 'widgets_init', 'film_register_widgets' );