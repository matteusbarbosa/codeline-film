<?php

//genre
function genre_init() {
	// create a new taxonomy
	register_taxonomy(
		'genre',
		'film',
		array(
			'label' => __( 'Genre' ),
			'rewrite' => array( 'slug' => 'genre' )		
		)
	);
}
add_action( 'init', 'genre_init' );

//country
function country_init() {
	// create a new taxonomy
	register_taxonomy(
		'country',
		'film',
		array(
			'label' => __( 'Country' ),
			'rewrite' => array( 'slug' => 'country' )		
		)
	);
}
add_action( 'init', 'country_init' );

//year
function year_init() {
	// create a new taxonomy
	register_taxonomy(
		'year',
		'unite',
		array(
			'label' => __( 'Year' ),
			'rewrite' => array( 'slug' => 'year' )		
		)
	);
}
add_action( 'init', 'year_init' );

//actor
function actor_init() {
	// create a new taxonomy
	register_taxonomy(
		'actor',
		'film',
		array(
			'label' => __( 'Actor' ),
			'rewrite' => array( 'slug' => 'actor' )		
		)
	);
}
add_action( 'init', 'actor_init' );