<?php

		// Register Custom Post Type
		function film_post_type() {

			$labels = array(
				'name'                  => _x( 'Film', 'Post Type General Name', 'unite' ),
				'singular_name'         => _x( 'Film', 'Post Type Singular Name', 'unite' ),
				'menu_name'             => __( 'Film', 'unite' ),
				'name_admin_bar'        => __( 'Film', 'unite' ),
				'archives'              => __( 'Item Archives', 'unite' ),
				'attributes'            => __( 'Item Attributes', 'unite' ),
				'parent_item_colon'     => __( 'Parent Item:', 'unite' ),
				'all_items'             => __( 'All Films', 'unite' ),
				'add_new_item'          => __( 'New Film', 'unite' ),
				'add_new'               => __( 'New', 'unite' ),
				'new_item'              => __( 'New', 'unite' ),
				'edit_item'             => __( 'Edit Film', 'unite' ),
				'update_item'           => __( 'Update', 'unite' ),
				'view_item'             => __( 'View', 'unite' ),
				'view_items'            => __( 'View Film', 'unite' ),
				'search_items'          => __( 'Search Film', 'unite' ),
				'not_found'             => __( 'Not Found', 'unite' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'unite' ),
				'featured_image'        => __( 'Featured Image', 'unite' ),
				'set_featured_image'    => __( 'Set featured image', 'unite' ),
				'remove_featured_image' => __( 'Remove featured image', 'unite' ),
				'use_featured_image'    => __( 'Use as featured image', 'unite' ),
				'insert_into_item'      => __( 'Insert into item', 'unite' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'unite' ),
				'items_list'            => __( 'Items list', 'unite' ),
				'items_list_navigation' => __( 'Items list navigation', 'unite' ),
				'filter_items_list'     => __( 'Filter items list', 'unite' )
			);
			$args = array(
				'label'                 => __( 'Film', 'unite' ),
				'description'           => __( 'Register films', 'unite' ),
				'labels'                => $labels,
				'taxonomies'			=> ['genre', 'coutry', 'year', 'actors'],
				'supports'              => ['title', 'editor', 'custom-fields'],
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => true,
				'menu_position'         => 1,
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'capability_type'       => 'post',
				'menu_icon' 			=> 'dashicons-format-video' 
			);

			register_post_type( 'film', $args );
		
        }
        
        if ( !post_type_exists( 'film' ) ) {
			//class-tryst-admin-meta.php
			add_action( 'init', 'film_post_type', 0 );
		}