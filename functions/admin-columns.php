<?php

// ADD NEW COLUMN
function columns_head($defaults) {

    if(get_post_type() != 'film')
    return;

    $defaults['country'] = 'Country';
    $defaults['genre'] = 'Genre';
    $defaults['ticket_price'] = 'Ticket Price';
    $defaults['release_date'] = 'Release Date';

    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function columns_content($column_name, $post_ID) {
    global $post;

    if(get_post_type() != 'film')
    return;

    if ($column_name == 'country') {
        admin_view_country_list($post);
    }

    if ($column_name == 'genre') {
        admin_view_genre_list($post);
    }

    if ($column_name == 'ticket_price') {
        admin_get_meta_ticket($post);
    }

    if ($column_name == 'release_date') {
        admin_get_meta_release_date($post);
    }
}

add_filter('manage_posts_columns', 'columns_head');
add_action('manage_posts_custom_column', 'columns_content', 10, 2);



    function admin_view_country_list($post){

        $countries = get_the_terms( $post->ID, 'country'); 
        if(!empty($countries)): 

            array_map(function($e) use ($countries)
            { 
                echo $e->name; 
                if($e != end($countries)) 
                echo ', '; 
            }, $countries); 
  
        endif;

    }

    function admin_view_genre_list($post){

        $genres = get_the_terms( $post->ID, 'genre'); 
        if(!empty($genres)): 
            array_map(function($e) use ($genres)
            { 
                echo $e->name; 
                if($e != end($genres)) 
                echo ', '; 
            }, $genres); 
    
        endif;

    }

    function admin_get_meta_ticket($post){
        $ticket_price = get_post_meta( $post->ID, 'ticket_price'); 
        if(!empty($ticket_price)): 
        echo current($ticket_price);
        endif;
    }

    function admin_get_meta_release_date($post){
        $release_date = get_post_meta( $post->ID, 'release_date'); 
        if(!empty($release_date)): 
        echo current($release_date);
        endif;
    }