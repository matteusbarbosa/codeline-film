<?php

function get_films( $atts ) {
    
    wp_reset_postdata();
    
    $atts = shortcode_atts( array(
        'count' => 1,
    ), $atts, 'get_films' );
    
    return load_films($atts['count']);
    
}

function load_films($count){
    
    $args = array(
        'numberposts' => $count,
        'post_type' => 'film',
        'orderby'          => 'date',
        'order'            => 'DESC'
    );
    
    $query_films = get_posts( $args );

    
    $html = '<h3 class="widget-title">Films Listing</h3>';
    $html .= '<ul>';
    
    foreach($query_films as $qf) :
        
        $html .= sprintf('<li><a href="%s">%s</a></li>', $qf->guid, $qf->post_title);
        
    endforeach; // end of the loop. 
    

    return $html.'</ul>';
}

add_shortcode( 'get_films', 'get_films' );