<?php
function get_view_content_extra($post){
    view_taxonomies_list($post);
    view_meta_list($post);
}
function view_meta_list($post){
    ?>
    <ul>
    <?php 
    $ticket_price = get_post_meta( $post->ID, 'ticket_price'); 
    if(!empty($ticket_price)): 
        ?>
        <li><strong>Ticket Price</strong>:  <?php echo current($ticket_price); ?>
        </li>
        <?php
    endif;
    ?>
    </li>
    <?php 
    $release_date = get_post_meta( $post->ID, 'release_date'); 
    if(!empty($release_date)): 
        ?>
        <li> <strong>Release Date</strong>: <?php echo current($release_date); ?>
        </li>
        <?php
    endif;
    ?>
    </ul>
    <?php
}
function view_taxonomies_list($post){
    ?>
    <ul>
    <?php 
    $countries = get_the_terms( $post->ID, 'country'); 
    if(!empty($countries)): 
        ?>
        <li><strong>Country</strong>: 
        <?php
        array_map(function($e) use ($countries)
        { 
            echo $e->name; 
            if($e != end($countries)) 
            echo ', '; 
        }, $countries); 
        ?>
        </li>
        <?php
    endif;
    ?>
    </li>
    <?php 
    $genres = get_the_terms( $post->ID, 'genre'); 
    if(!empty($genres)): 
        ?>
        <li><strong>Genres</strong>:
        <?php
        array_map(function($e) use ($genres)
        { 
            echo $e->name; 
            if($e != end($genres)) 
            echo ', ';
        }, $genres); 
        ?>
        </li>
        <?php
    endif;
    ?>
    </ul>
    <?php
}