<?php
function theme_enqueue_styles() {
    $parent_style = 'parent-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
    get_stylesheet_directory_uri() . '/style.css',
    array( $parent_style )
);
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
/* 
*
*/
require __DIR__.'/functions/taxonomies.php';
require __DIR__.'/functions/post-types.php';
require __DIR__.'/functions/single.php';
require __DIR__.'/functions/shortcodes.php';
require __DIR__.'/functions/widgets.php';

if(is_admin()){
    require __DIR__.'/functions/admin-columns.php';
}